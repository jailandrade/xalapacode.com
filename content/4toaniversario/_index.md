+++
title = "4to aniversario"
+++

## xalapacode: 4 años de un viaje

![Imagen 4to aniversario xalapacode](/img/xalapacode/4oaniv.png)

Este mes cumplimos 4 años del primer meetup de lo que inicio como xalapajs y hoy es la comunidad de tecnologia mas grande de Xalapa

## 2015

El 7 de Octubre asisto con unos amigos a ChelaJS y quedo intoxicado, por el alcohol y por el nivel técnico de las charlas. Y es entonces cuando decidimos crear @xalapajs: una comunidad para programadores web en Xalapa y poder organizar eventos continuos.

![Imagen del primer tuit de la comunidad](/img/xalapacode/4oaniv/tuit-inicio.png)

En ese entonces tenia a Leo de cofundador

![Imagen de primer tuit de Leo sobre la comunidad](/img/xalapacode/4oaniv/tuit-leo.png)

El primer encuentro fue en Don Justo café

![Imagen tuit de primer encuentro en Don Justo](/img/xalapacode/4oaniv/tuit-encuentro-don-justo.png)

El primer meetup con charlas fue en Estación 31

![Imagen de primer meetup en estación 31](/img/xalapacode/4oaniv/tuit-primer-meetup.png)

La primer platica la dio Leo

![!Imagen de primera platica por Leo](/img/xalapacode/4oaniv/primer-platica.png)

La segunda platica la di yo

![!Imagen de segunda platica del primer meetup por Jail](/img/xalapacode/4oaniv/segunda-platica.png)

El meetup de diciembre se cancela y hacemos otro encuentro

![Imagen de tuit de encuentro diciembre 2015](/img/xalapacode/4oaniv/meetup-diciembre-2015.png)

## 2016

El primer hecho clave para que sobrevivieramos como comunidad es que logramos dar un taller en FLISoL Xalapa 2016

![Imagen de taller xalapajs en el FLISoL 2016](/img/xalapacode/4oaniv/taller-angularjs-2016.png)

Eso nos permitio hacer meetup en la FEIUV en Mayo

![Imagen de meetup de Mayo en la FEIUV](/img/xalapacode/4oaniv/meetup-mayo-2016.png)

![Imagen de platica de Leo en el meetup de Mayo](/img/xalapacode/4oaniv/meetup-mayo-2016-leo.png)

Tambien fue la primera ocasión en la que pudimos ayudar a alguien a encontrar chamba

![Imagen de primer retuir laboral](/img/xalapacode/4oaniv/primer-retuit-recomendacion-laboral.png)

Comenzamos a tener mas miembros recurrentes

![Imagen del primer tuit de Cesar promoviendonos](/img/xalapacode/4oaniv/cesar-se-unio.png)

Tuvimos un meetup en Junio

![Imagen meetup junio 2016](/img/xalapacode/4oaniv/meetup-junio-2016.png)

Ultimo meetup del año, en Septiembre, aqui comenzamos con los carteles

![Imagen del ultimo meetup de 2016](/img/xalapacode/4oaniv/meetup-septiembre-2016.png)

## 2017

Tuvimos el primer meetup del año en Febrero

![Imagen de tuit de meetup de febrero de 2017](/img/xalapacode/4oaniv/meetup-febrero-2017.png)

Y en Marzo organizamos el primer SHDH

![Imagen cartel primer SHDH](/img/xalapacode/4oaniv/primer-shdh-2017.png)

En Abril volvimos a colaborar en el FLISOL

![Imagen de Flisol 2017](/img/xalapacode/4oaniv/flisol-2017.png)

En Junio volvimos a tener meetup

![Imagen de cartel de meetup junio 2017](/img/xalapacode/4oaniv/meetup-junio-2017.jpg)
![Imagen de de meetup junio 2017 de Mario](/img/xalapacode/4oaniv/meetup-junio-2017-mario.jpg)
![Imagen de de meetup junio 2017 de Leo](/img/xalapacode/4oaniv/meetup-junio-2017-leo.jpg)
![Imagen de de meetup junio 2017 de Abraham](/img/xalapacode/4oaniv/meetup-junio-2017-cate.jpg)
![Imagen de de meetup junio 2017 de Jail](/img/xalapacode/4oaniv/meetup-junio-2017-jail.jpg)

Y se dieron talleres

![Imagen de cartel de taller PHP 2017](/img/xalapacode/4oaniv/taller-php-2017.jpg)
![Imagen del taller de PHP 2017](/img/xalapacode/4oaniv/taller-php-mario.jpg)

![Tambien tuvimos un chat de gitter que no funciono, igual que el slack](/img/xalapacode/4oaniv/gitter.png)

De ahi hubo una breve pausa que nos trajo la renovación de nuestra identidad:

![Nuevo logo](/img/xalapacode/logo.png)

El ultimo meetup de 2017 fue en noviembre

![Cartel de Meetup de Noviembre 2017 con nuevo logo](/img/xalapacode/4oaniv/meetup-noviembre-2017.jpg)


## 2018

En 2018 volvimos a la carga con mas eventos

Primero tuvimos un meetup

![Primer meetup del año en Marzo 2018](/img/xalapacode/4oaniv/meetup-marzo-2018.jpg)

Luego un SHDH

![SHDH de Marzo en 2018](/img/xalapacode/4oaniv/shdh-marzo-2018.jpg)

Tambien realizamos un installfest

![Imagen de cartel del Installfest 2018](/img/xalapacode/4oaniv/installfest-2018.jpg)

![Imagen de Abraham dando taller remotamente](/img/xalapacode/4oaniv/taller-installfest-cate.jpg)

Pero todo se puso serio en Octubre, llamemosle el evento que transformo la comunidad

![Imagen de cartel de Meetup de Octubre 2018](/img/xalapacode/4oaniv/meetup-octubre-2018.jpg)
![Imagen del meetup de Octubre 2018](/img/xalapacode/4oaniv/meetup-octubre-18a.jpg)
![Imagen del meetup de Octubre 2018](/img/xalapacode/4oaniv/meetup-octubre-18b.jpg)
![Imagen del meetup de Octubre 2018](/img/xalapacode/4oaniv/meetup-octubre-18c.jpg)

Luego en Noviembre consolidamos el evento

![Otro meetup continuo](/img/xalapacode/4oaniv/meetup-noviembre-2018.jpg)

Terminamos 2018 con mas miembros y mas historias para contar

![Imagen de audiencia meetup diciembre](/img/xalapacode/4oaniv/meetup-diciembre-casa-llena.jpg)

## 2019

Y asi llegamos a principio de año y nuestro meetup mas grande jamas realizado

![Imagen de cartel de meetup enero 2019](/img/xalapacode/4oaniv/meetup-enero-2019.jpg)
![Empezamos con mucha gente](/img/xalapacode/4oaniv/meetup-enero-2019-gente.png)
![Dia de la programadora](/img/xalapacode/4oaniv/dia-programadora.jpg)
![Imagen de dia de muertos](/img/xalapacode/4oaniv/dia-de-muertos.jpg)

## Moralejas de este viaje

* No importa que a tus eventos solo vayan 2 personas (la que presta el lugar y tú que organizas)

* Incluso no importa si tú eres esa misma persona (la que presta el lugar y la que organiza)

* Lo importante es ser constante; establece una fecha y horario al que la mayoria pueda asistir

* Ve a otros eventos y encuentros y pide chance para difundir tu evento (funciona mejor que con redes sociales)

* Se cercano a tus colaboradores, saluda a todos los asistentes

* Da stickers, a la gente le encanta los stickers

* Colabora con otras comunidades

* Se humilde y agradecido.

* Toma muchas fotos, pide a los asistentes que compartan con el #xalapacode en twitter y facebook para tener fotos


Gracias totales
