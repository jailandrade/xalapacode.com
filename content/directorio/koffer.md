+++
name = "Iván Mejía"
nickname = "Koffer"
description = "Diseñador a ratos, Organizador y creador de sitios web en otros momentos. Gusta de trabajar con el CMS Drupal, De bailar y tomar fotografías"
skills = "HTML, CSS, Drupal"
thumbnail = "/img/directorio/koffer.png"
draft = false
date = "2024-08-22"
disponibility = "Trabajo en linea"
rol = "root"
website = "http://medioyforma.info"
contacto = "https://www.linkedin.com/in/koffermx"
author = "koffer"
+++

Linkedin: https://www.linkedin.com/in/koffermx/

Ivan Mejia también conocido como koffer, lleva más de diez años construyendo sitios web de muy diverso tamaño. Al inicio con HTML y CSS, despues usando el CMS Drupal.

Estudió la Licenciatura en Comunicación Grafica en la Escuela Nacional de Artes Plásticas de la UNAM. Los sitios web más sobresalientes son proyectos para ONU/CEPAL, Instituto de Investigaciones en Materiales/UNAM, Hoteles Emporio, Altexto.

Durante 5 años fue socio de la empresa Bcomsulting dedicada al desarrollo de contenido para internet, atendiendo a clientes como son
Grupo posadas y Televisa.

Actualmente desarrolla sitios web en el cms drupal y participa activamente en comunidades de software libre.
