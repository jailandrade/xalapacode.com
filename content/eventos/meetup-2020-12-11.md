+++
title = "Encuentro virtual del 11 de diciembre, 2020"
publishdate = "2020-12-07"
date = "2020-12-11"
event_place = "En línea"
description = "Último evento del año 2020!"
thumbnail = "/img/eventos/meetup-2020-12-11.png"
author = "categulario"
draft = false
+++

## Las pláticas

1. **Cosas que deberías saber sobre infraestructura**, por David Barreda
2. **Introducción a la inyección de código**, por Gerardo Contreras
3. **Comunidad: una perspectiva**, por Víctor Hernández

## Sobre la sede

Debido a la contingencia sanitaria que se vive a nivel mundial nuestros últimos eventos han sido (y seguirán siendo) en línea, ¡así que ahora es más fácil que nunca asistir!

Sigue la transmisión en vivo por cualquiera de nuestros canales:

* [Youtube](https://www.youtube.com/channel/UCR7II0ABLovdY_vfBApiHCg)
* [Facebook live](https://www.facebook.com/xalapacode)
* [Twitter](https://twitter.com/xalapacode)

## Sobre el evento

Los encuentros de xalapacode son reuniones en las que se realizan una serie de desconferencias de 15 minutos de duración por miembros de la comunidad como tú sobre temas de ciencia, tecnología, cultura y sociedad. Estos eventos son gratuitos y libres. Al finalizar el evento no te olvides de saludar a los ponentes y a los asistentes para involucrate más en la comunidad.
