+++
title = "Meetup 8 de Noviembre 2019"
publishdate = "2019-10-14"
date = "2019-11-08"
event_place = "Mosaico Coworking"
description = "El evento del 4to aniversario"
thumbnail = "/img/eventos/meetup-2019-11-08.png"
author = "categulario"
draft = false
turnout = 22
+++

## Las pláticas

1. **¿Cómo me volví un programador estando en otro país?**, por Ivan Leon
2. **Tus microservicios en caldo de iguana**, por Categulario
3. **XalapaCode: un viaje**, por Jail

## Sobre la sede

En esta ocasión y por primera vez nos recibe **Mosaico Coworking**, ubicado en Privada Adalberto Tejeda 14, colonia Pumar, Xalapa, Veracruz.

[Mapa](https://maps.google.com/?cid=13120181369142633783)

## Sobre el evento

Los meetups de xalapacode son encuentros en los que se realizan una serie de desconferencias de 15 minutos de duración por miembros de la comunidad como tu sobre temas de ciencia, tecnología, cultura y sociedad. Estos eventos son gratuitos y libres. Al finalizar el evento no te olvides de saludar a los ponentes y a los asistentes para involucrate más en la comunidad.
