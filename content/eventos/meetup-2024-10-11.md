+++
title = "Xalapacode Octubre/2024"
publishdate = "2024-10-02"
date = "2024-10-11"
event_place = "Universidad Veracruzana - Facultad de Estadística e Informática - Salón Audiovisual"
description = "Vamos a hablar sobre Seguridad, uso de APIs para entrega de datos y CDNs (más allá de React)"
thumbnail = "/img/eventos/meetup-2024-10-11.png"
author = "Ivan Leon"
draft = false
+++

## Sobre el evento

- Estaganografia, permite ocultar mensajes en objetos y medios, de manera digital o en una simple hoja de papel. Desde la antiguedad, se emplean dichas técnicas, y al día de hoy, organizaciones de todos los tipos, utilizan esta técnica en medios digitales.

- APIs REST, son ampliamente utilizadas, pero hay otras arquitecturas de webservices para entrega de datos. GraphQL, está presente hace un par de años, con un concepto de entrega de datos específico, más robusto. Veamos que se puede hacer con GraphQL, para el desarrollo de plataformas y servicios.

- Sea usando React, Bootstrap, jQuery, dichas librerías y servicios, están disponibles por medio de una CDN: algo que poco se sabe, además en el mundo del desarrollo de Frontend. Vamos hablar un poco del "inframundo" que permite el uso de esta arquitectura, y por que ella es muy importante.

Vamos a traer panes para compartir, distribuir SWAGs, y hacer un sorteo :).

Si ya nos conocemos, va ser un gusto verte aquí una vez más! Y sino, pues bienvenido a la comunidad!

## Sobre la sede

Te esperamos en el Salón Audiovisual de la FEI (Facultad de Estadística e informática), a las 19:00 horas (horário de la Ciudad de México).

Ubicación de la FEI: https://goo.gl/maps/HrK9GnHB7XgycFnm6


¡Nos vemos pronto!
