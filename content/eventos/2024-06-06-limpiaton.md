+++
title = "Limpiatón de rutas de autobús"
publishdate = "2024-06-02"
date = "2024-06-06"
event_place = "Salón 108, FEI UV"
description = "Evento de limpieza de datos recaudados durante el mapatón Xalapa 2016"
thumbnail = "/img/eventos/2024-06-06_limpiaton.jpg"
author = "categulario"
draft = false
+++

En 2016 se hizo un gran evento llamado
[mapatón](https://mapaton.org/mapaton-ciudadano-xalapa/) donde se recopilaron
datos en campo de las rutas de autobús de la ciudad. El resultado del esfuerzo
fue un [compendio de trazas](https://gitlab.com/xalapacode/xalapa_bus_data) que
a la fecha siguen sin procesar. Nuestro objetivo es limpiarlas, etiquetarlas y
generar un archivo más fácil de utilizar.

Durante el evento aprenderás un poquito de Qgis (solo lo necesario para trabajar
las rutas) y conocerás el resultado de ese ejercicio de hace 8 años. Con tu
apoyo podríamos lograr tener un compendio bien etiquetado a partir de las 120
trazas que se registraron.

## Requisitos

Hay dos formas en las que puedes participar: *Etiquetando* y *Limpiando*. Para
etiquetar lo único que necesitas es tener conocimiento de un puñado de rutas de
autobús. Lo usaremos para reconocer sus rutas, identificarlas y etiquetarlas de
entre las del archivo. El rol de limpieza es un poco más técnico pero aun así
accesible a cualquiera que pueda traer:

* Laptop
* Mouse o tableta gráfica

El mouse es importante porque mapear con trackpad es una gran molestia. Durante
el evento instalaremos el software Qgis que es lo que usaremos para corregir las
trazas.

## Registro

En la siguiente liga:

https://nextcloud.categulario.xyz/apps/forms/s/zCqx7RnnBzPcszNkrTszaQmz

## Sobre la sede

Nos veremos en el salón 8 de la Facultad de Estadística e Informática, tamibién
conocida como "Economía" en los letreros de algunos autobuses, que está en la
esquina de Ávila Camacho y Av. Xalapa.

[Mapa](https://goo.gl/maps/jSVYkro2biDL7wD16)
