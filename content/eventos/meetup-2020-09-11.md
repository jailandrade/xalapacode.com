+++
title = "Encuentros xalapacode septiembre 2020"
publishdate = "2020-09-07"
date = "2020-09-11"
event_place = "Nuestras redes sociales"
description = "Nuestro evento estela de agosto con los mejores ponentes del mundo"
thumbnail = "/img/eventos/meetup-2020-09-11.png"
author = "jail"
draft = false
+++

Nuestro evento estelar de septiembre va a ser espectacular, trabajamos para volvernos la comunidad que nuestros miembros esperan y merecen.

## Las pláticas registradas

* "Más allá de TDD, técnicas avanzadas para escribir buen software" por [@categulario](https://twitter.com/categulario)
* "Google Cloud, Serverless y Python" por [@ivanleoncz](https://twitter.com/ivanleoncz)
* "Introducción a las arquitecturas de aprendizaje profundo" por [@AlexMaldonad0](https://twitter.com/AlexMaldonad0)

## Sobre la transmisión del evento

El evento sera transmitido simultaneamente por nuestros siguientes canales:

* [Streaming periscope](https://www.twitter.com/xalapacode)
* [Streaming youtube](https://www.youtube.com/watch?v=8Ath6T_SjFs)
* [Streaming facebook](https://www.facebook.com/xalapacode/videos/3415804525148001/)

## Sobre el evento

Los encuentros de xalapacode son reuniones en las que se realizan una serie de desconferencias de 20 minutos de duración por miembros de la comunidad como tú sobre temas de ciencia, tecnología, cultura y sociedad. Estos eventos son gratuitos y libres. Al finalizar el evento no te olvides de saludar a los ponentes y a los asistentes para involucrate más en la comunidad.
